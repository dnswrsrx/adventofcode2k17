import unittest
from day3part1_solution import taxicab

class Day3_Part1_Tests(unittest.TestCase):

    def test_1(self):
        self.assertEqual(taxicab(1), 0.0)

    def test_2(self):
        self.assertEqual(taxicab(12), 3.0)

    def test_3(self):
        self.assertEqual(taxicab(23), 2.0)

    def test_4(self):
        self.assertEqual(taxicab(1024), 31.0)

if __name__ == '__main__':
    unittest.main()
