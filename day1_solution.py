def compile_matches_and_sum_1(numbers):
    numbers += numbers[0]
    return sum([int(numbers[i]) for i in range(0, len(numbers)-1)
        if numbers[i] == numbers[i+1]])

def compile_matches_and_sum_2(numbers):
    index_to_skip = int(len(numbers)/2)
    return sum([int(numbers[i]) for i in range(0, index_to_skip)
        if numbers[i] == numbers[i+index_to_skip]]) * 2

if __name__ == '__main__':
    with open('day1input.txt', 'r') as input_file:
        input_file = input_file.read().replace('\n', '')
    print('Answer for part 1 is %d' % compile_matches_and_sum_1(input_file))
    print('Answer for part 2 is %d' % compile_matches_and_sum_2(input_file))
