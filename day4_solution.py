# Part 1
def count_valid_passphrases_1(collection):
    return ['Valid' if len(set(line)) == len(line)
              else 'Not Valid'
              for line in collection].count('Valid')

# Part 2
def valid_if_no_anagram(line):
    line_with_reordered_words = list(map(
        lambda word: ''.join(sorted(word)), line))
    if len(line) == len(set(line_with_reordered_words)):
        return 'Valid'
    return 'Invalid'

def count_valid_passphrases_2(collection):
    return [valid_if_no_anagram(row) for row in collection].count('Valid')

if __name__ == '__main__':
    with open('day4input.txt', 'r') as file:
        sheet = [[word for word in lines.split()] for lines in file]
    print('Result for part 1 is %d' % count_valid_passphrases_1(sheet))
    print('Result for part 2 is %d' % count_valid_passphrases_2(sheet))
