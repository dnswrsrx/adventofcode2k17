import unittest
from day4_solution import *

TEST_SAMPLE_1 = [['aa', 'bb', 'cc', 'dd', 'ee']]
TEST_SAMPLE_2 = [['aa', 'bb', 'cc', 'dd', 'aa']]
TEST_SAMPLE_3 = [['aa', 'bb', 'cc', 'dd', 'aaa']]

class Day4_Part1_Tests(unittest.TestCase):
    def test_1(self):
        self.assertEqual(count_valid_passphrases_1(TEST_SAMPLE_1), 1)

    def test_2(self):
        self.assertEqual(count_valid_passphrases_1(TEST_SAMPLE_2), 0)

    def test_3(self):
        self.assertEqual(count_valid_passphrases_1(TEST_SAMPLE_3), 1)

TEST_SAMPLE_7 = ['abc', 'cba']
TEST_SAMPLE_8 = ['abc', 'def']
TEST_SAMPLE_9 = ['abc', 'def', 'fed']

class Day4_Part2_Tests(unittest.TestCase):

    def test_check_passphrase_for_anagram_1(self):
        self.assertEqual(valid_if_no_anagram(TEST_SAMPLE_7), 'Invalid')

    def test_valid_if_no_anagram_2(self):
        self.assertEqual(valid_if_no_anagram(TEST_SAMPLE_8), 'Valid')

    def test_valid_if_no_anagram_3(self):
        self.assertEqual(valid_if_no_anagram(TEST_SAMPLE_9), 'Invalid')

    def test_count_valid_passphrases_2_1(self):
        self.assertEqual(count_valid_passphrases_2([TEST_SAMPLE_7]), 0)

    def test_count_valid_passphrases_2_2(self):
        self.assertEqual(count_valid_passphrases_2([TEST_SAMPLE_8]), 1)

    def test_count_valid_passphrases_2_3(self):
        self.assertEqual(count_valid_passphrases_2([TEST_SAMPLE_9]), 0)

if __name__ == '__main__':
    unittest.main()
