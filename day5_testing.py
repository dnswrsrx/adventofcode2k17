import unittest
from day5_solution import *

TEST_SAMPLE = [0,3,0,1,-3]

class Day5_Part1_Tests(unittest.TestCase):

    def test_1(self):
        self.assertEqual(turns_to_escape_maze_1(TEST_SAMPLE), 5)

class Day5_Part2_Tests(unittest.TestCase):

    def test_offset_index_4(self):
        self.assertEqual(offset_index(4), -1)

    def test_offset_index_1(self):
        self.assertEqual(offset_index(1), 1)

    def test_2(self):
        self.assertEqual(turns_to_escape_maze_2(TEST_SAMPLE), 10)

if __name__ == '__main__':
    unittest.main()
