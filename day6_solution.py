from random import randint

def next_index(index, numbers):
    if index < len(numbers)-1:
        return index + 1
    else:
        return 0

def redistribute(numbers):
    copy = list(numbers)
    index = numbers.index(max(numbers))
    number_for_redistribution = copy[index]
    copy[index] = 0
    while number_for_redistribution > 0:
        index = next_index(index, copy)
        copy[index] += 1
        number_for_redistribution -= 1
    return copy

def repeat_redistribution(numbers):
    history = [numbers]
    turn = 0
    while history.count(history[-1]) == 1:
        history.append(redistribute(history[-1]))
        turn += 1
    return history, turn

def number_of_redistribution(numbers):
    history, turn = repeat_redistribution(numbers)
    return (turn, turn-history.index(history[-1]))

if __name__ == '__main__':
    with open('day6input.txt', 'r') as input_file:
        input_file = input_file.read().split('\t')
        input_file = [int(number.strip()) for number in input_file]
    output_1, output_2 = number_of_redistribution(input_file)
    print('Answer to part 1: {}'.format(output_1))
    print('Answer to part 2: {}'.format(output_2))
