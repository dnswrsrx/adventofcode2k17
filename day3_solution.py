import numpy as np


class Cell:
    def __init__(self, number):
        self.number = int(number)
        self.value = 1
        self.ring_level = int(np.floor(np.ceil(np.sqrt(self.number)) / 2))
        self.side_length = (self.ring_level * 2) + 1
        self.corners = self.corners()
        self.middles = self.middles()
        self.side = self.side_of_value()
        self.coordinates = self.coordinates()
        self.neighbours = self.neighbouring_coordinates()

    def corners(self):
        bottom_right = self.side_length ** 2
        return [bottom_right - side*(self.side_length-1)
                for side in range(0, 4)]

    def middles(self):
        bottom_mid = max(self.corners) - self.ring_level
        sides = ['bottom', 'left', 'top', 'right']
        mids =  [bottom_mid - side*(self.side_length-1)
                for side in range(0, 4)]
        return {side: mid for side, mid in zip(sides, mids)}

    def side_of_value(self):
        sides = ['bottom', 'left', 'top', 'right']
        value_ranges = [(corner, corner-self.side_length+1)
                for corner in self.corners]
        side_ranges = {side: value for side, value in zip(sides, value_ranges)}
        side = [side for side in side_ranges if side_ranges[side][0]
                >= self.number >= side_ranges[side][1]][0]
        return side

    def coordinate_from_mid(self):
        coordinate = {
                'top': -(self.number - self.middles['top']),
                'bottom': self.number - self.middles['bottom'],
                'left': -(self.number - self.middles['left']),
                'right': self.number - self.middles['right']
                }
        return coordinate[self.side]

    def coordinates(self):
        coordinates = {
                'top': {'y': self.ring_level,
                    'x': self.coordinate_from_mid()},
                'bottom': {'y': -self.ring_level,
                    'x': self.coordinate_from_mid()},
                'left': {'y': self.coordinate_from_mid(),
                    'x': -self.ring_level},
                'right': {'y': self.coordinate_from_mid(),
                    'x': self.ring_level}
                }
        return(coordinates[self.side])

    def neighbouring_coordinates(self):
        neighbours = [{'y': new_y, 'x': new_x}
                for new_y in range(self.coordinates['y']-1,
                    self.coordinates['y']+2)
                for new_x in range(self.coordinates['x']-1,
                    self.coordinates['x']+2)
                if {'y': new_y, 'x': new_x} != self.coordinates]
        return neighbours

    def assign_value(self, grid):
        neighbour_values = [cell.value for cell in grid if
                cell.coordinates in self.neighbours]
        return sum(neighbour_values)


def taxicab(number):
    cell = Cell(number)
    distance_to_mid = [abs(cell.number - mid)
            for mid in list(cell.middles.values())]
    return cell.ring_level + min(distance_to_mid)


def build_grid(number):
    grid = [Cell(1)]
    current_number = grid[-1].number
    while grid[-1].value < number:
        current_number += 1
        grid.append(Cell(current_number))
        grid[-1].value = grid[-1].assign_value(grid)
        #print([cell.value for cell in grid])
    return(grid[-1].value)


if __name__ == '__main__':
    user_input = int(input("Number: "))
    print('The answer to part 1: {}'.format(taxicab(user_input)))
    print('The answer to part 2: {}'.format(build_grid(user_input)))
