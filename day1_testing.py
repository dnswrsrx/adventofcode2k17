import unittest
from day1_solution import compile_matches_and_sum_1, compile_matches_and_sum_2

TEST_SAMPLE_1 = '1122'
TEST_SAMPLE_2 = '1111'
TEST_SAMPLE_3 = '1234'
TEST_SAMPLE_4 = '91212129'
TEST_SAMPLE_5 = '1212'
TEST_SAMPLE_6 = '1221'
TEST_SAMPLE_7 = '123425'
TEST_SAMPLE_8 = '123123'
TEST_SAMPLE_9 = '12131415'

class Day1_Part1_Tests(unittest.TestCase):

    def test_1(self):
        self.assertEqual(compile_matches_and_sum_1(TEST_SAMPLE_1), 3)

    def test_2(self):
        self.assertEqual(compile_matches_and_sum_1(TEST_SAMPLE_2), 4)

    def test_3(self):
        self.assertEqual(compile_matches_and_sum_1(TEST_SAMPLE_3), 0)

    def test_4(self):
        self.assertEqual(compile_matches_and_sum_1(TEST_SAMPLE_4), 9)

class Day1_Part2_Tests(unittest.TestCase):
    def test_1(self):
        self.assertEqual(compile_matches_and_sum_2(TEST_SAMPLE_5), 6)

    def test_2(self):
        self.assertEqual(compile_matches_and_sum_2(TEST_SAMPLE_6), 0)

    def test_3(self):
        self.assertEqual(compile_matches_and_sum_2(TEST_SAMPLE_7), 4)

    def test_4(self):
        self.assertEqual(compile_matches_and_sum_2(TEST_SAMPLE_8), 12)

    def test_5(self):
        self.assertEqual(compile_matches_and_sum_2(TEST_SAMPLE_9), 4)


if __name__ == '__main__':
    unittest.main()


